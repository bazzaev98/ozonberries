package ru.bazzaev.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "orders")
public class Order extends AbstractEntity{

    @Column(name = "id_product")
    private Integer idProd;
    @Column(name = "id_users")
    private Integer idUser;
}
