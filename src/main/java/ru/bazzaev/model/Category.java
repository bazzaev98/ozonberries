package ru.bazzaev.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "category")
public class Category extends AbstractEntity{

    @Column
    private String name;
    @Column
    private String description;
    @Column(name = "parent_id")
    private Integer parentId;
    @ManyToOne
    @JoinColumn(name = "shop_id")
    private Shop shop;


    
}
