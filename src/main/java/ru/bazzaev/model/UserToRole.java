package ru.bazzaev.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "users_roles")
public class UserToRole extends AbstractEntity {

   @Column(name = "user_id")
    private Integer userId;
    private Integer roleId;

}
