package ru.bazzaev.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "shop")
public class Shop extends AbstractEntity{
   @Column
    private String description;
   @Column
    private String name;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private ShopStatus status;

    public enum ShopStatus{
        ACTIVE,
        PASSIVE
    }

}
