package ru.bazzaev.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "users")
public class User extends AbstractEntity {

    @Column(name = "name")
    private String username;

    @Column
    private String email;

    @Column
    private String password;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private User.UserStatus status;

    public enum UserStatus {
        ACTIVE,
        PASSIVE
    }
}
