package ru.bazzaev.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "product")
public class Product extends AbstractEntity{

    @Column
    private String name;
    @Column
    private String description;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
    @Column
    private Integer price;
    @Column
    private Integer count;
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private ProductStatus status;

    public enum ProductStatus {
        ACTIVE,
        PASSIVE
    }
}
