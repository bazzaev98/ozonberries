package ru.bazzaev.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.bazzaev.model.Category;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    @Query("select c from Category c " +
            "where c.shop.id = :shopId " +
            "AND c.name = :name")
    Optional<Category> findByNameAndShop(String name, Integer shopId);

    @Query("select c from Category c " +
            "where (c.parentId is null " +
            "AND :parentId is null " +
            "AND c.shop.id = :shopId) " +
            "OR c.parentId = :parentId " +
            "AND c.shop.id = :shopId ")
    List<Category> findByShopOrCategoryId(Integer shopId, Integer parentId);

    @Query("select c from Category c " +
            "where c.shop.id = :shopId " )
    List<Category> findByShopId(Integer shopId);

    @Modifying
    @Query("delete from Category c " +
            "where c.id = :categoryId " +
            "or c.parentId = :categoryId")
    void deleteByCategoryId(Integer categoryId);

    @Query(nativeQuery = true, value =
            "with recursive r AS (" +
                    "select id from  category " +
                    "where id = :categoryId " +
                    "UNION " +
                    "select c.id from category c " +
                    "join r " +
                    "on c.parent_id = r.id ) " +
                    "select * from r ")
    List<Integer> findByCategoryAndParentCategory(Integer categoryId);

}
