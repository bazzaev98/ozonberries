package ru.bazzaev.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.bazzaev.model.Role;

import java.util.Optional;
@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    String ADMINISTRATOR = "administrator";
    String SELLER = "seller";
    String CUSTOMER = "customer";

    @Query(nativeQuery = true, value =
            "select * from role s " +
                    "where :name = name")
    Optional<Role> findByName(String name);
}
