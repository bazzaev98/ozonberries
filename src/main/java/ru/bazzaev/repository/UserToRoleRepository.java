package ru.bazzaev.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.bazzaev.model.UserToRole;

import java.util.Optional;

@Repository
public interface UserToRoleRepository extends JpaRepository<UserToRole, Integer> {

    Optional<UserToRole> findByUserIdAndRoleId(Integer userId, Integer roleId);


    @Modifying
    @Query("delete from UserToRole " +
           "where  userId = :userId ")
    void deleteByUserId(Integer userId);
}
