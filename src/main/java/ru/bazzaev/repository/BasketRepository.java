package ru.bazzaev.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.bazzaev.model.Basket;

import java.util.List;
import java.util.Optional;

public interface BasketRepository extends JpaRepository<Basket, Integer> {
    @Query(nativeQuery = true, value =
            " select * from basket b " +
                    " where (:userId is null or user_id = :userId ) ")
    List<Basket> findByBasket(Integer userId);


    @Query(nativeQuery = true, value =
            " select * from basket b " +
                    " where user_id = :userId " +
                    " AND (:productId is null or product_id = :productId) ")
    List<Basket> findByUserId(Integer userId, Integer productId);


    @Modifying
    @Query(nativeQuery = true, value =
            " delete from basket b " +
                    " where user_id = :userId ")
    Integer deleteByUserId(Integer userId);



    @Query(nativeQuery = true, value =
            " select * from basket b " +
                    " where user_id = :userId " +
                    " and  product_id = :productId ")
    Basket removeByBasket(Integer productId, Integer userId);

    @Query(nativeQuery = true, value =
            " select * from basket b " +
                    " where user_id = :userId " +
                    " and  product_id = :productId ")
    Optional <Basket> findByProduct(Integer productId, Integer userId);

    @Query(nativeQuery = true, value =
            " select * from basket b " +
                    " where user_id = :userId " +
                    " and  product_id = :productId ")
     Basket findByBasketProduct(Integer productId, Integer userId);
}