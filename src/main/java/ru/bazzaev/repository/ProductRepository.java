package ru.bazzaev.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.bazzaev.model.Product;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    Optional<Product> findByNameAndCategoryId(String name, Integer categoryId);
    @Query(" select p" +
            " from Product p " +
            " inner join p.category c " +
             "where (:categoryId is null or :categoryId = c.id) " +
              "AND (:productName is null or lower (p.name) LIKE %:productName%) " +
              "AND (:priceMin is null or p.price >= :priceMin) " +
              "AND (:priceMax is null or p.price <= :priceMax) ")
    List<Product> findByFilter(String productName, Integer categoryId,
                               Integer priceMin, Integer priceMax);

    @Query(nativeQuery = true, value =
            " select * from product p " +
                    " left join category c on " +
                    " p.category_id =  c.id " +
                    " left join shop s on " +
                    " c.shop_id = s.id " +
                    "where (s.id  = :shopId) ")
    List<Product> findByFilterProductShop(Integer shopId);

    @Modifying
    @Query("delete from Product p " +
            "where p.category.id = :categoryId ")
    void deleteByCategoryId(Integer categoryId);


    @Query(nativeQuery = true, value =
            " select * from product p " +
                    "where p.id  = :productId ")
    Product findByProductId(Integer productId);

    @Query(nativeQuery = true, value =
            " select * from product p " +
                    "where (p.category_id  = :categoryId) ")
    List<Product> findByCategoryId(Integer categoryId);





}
