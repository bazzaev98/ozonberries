package ru.bazzaev.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.bazzaev.model.User;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findByEmail(String name);

    @Query(nativeQuery = true, value =
            " select * from users u " +
                    "where u.id  = :userId ")
    User findByUserId(Integer userId);

}
