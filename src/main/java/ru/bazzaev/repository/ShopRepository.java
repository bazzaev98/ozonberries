package ru.bazzaev.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.bazzaev.model.Shop;

import java.util.List;
import java.util.Optional;

@Repository
public interface ShopRepository extends JpaRepository<Shop, Integer> {

    Optional<Shop> findByName(String name);

    @Query("select s from Shop s " +
                    "where :name is null or lower (s.name) LIKE %:name% ")
    List<Shop> findByNames(String name);



    @Modifying
    @Query(nativeQuery = true, value =
            "delete from shop s " +
            "where user_id = :userId ")
    void deleteByUserId(Integer userId);

    @Query(nativeQuery = true, value =
            "select * from shop s " +
                    "where user_id = :userId " +
                    "and id = :shopId ")
    List<Shop> findByUserAndId(Integer shopId, Integer userId);

    @Query(nativeQuery = true, value =
            "select * from shop s " +
                    "where user_id = :userId " )
    List<Shop> findByUserId (Integer userId);
}
