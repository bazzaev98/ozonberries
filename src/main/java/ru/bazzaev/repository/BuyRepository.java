package ru.bazzaev.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.bazzaev.model.Buy;

public interface BuyRepository extends JpaRepository<Buy, Integer> {


    @Modifying
    @Query(nativeQuery = true, value =
            " delete from buy b " +
                    " where user_id = :userId ")
    void deleteByUserId(Integer userId);

}
