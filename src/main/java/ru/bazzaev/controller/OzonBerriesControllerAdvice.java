package ru.bazzaev.controller;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class OzonBerriesControllerAdvice {

    @ExceptionHandler(Exception.class)
    public String handException(Exception e){
        return e.getMessage();
    }
}
