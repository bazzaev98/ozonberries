package ru.bazzaev.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.bazzaev.dto.UserRegistrationRequestDto;
import ru.bazzaev.model.*;
import ru.bazzaev.service.OzonBerriesService;

import java.util.List;


@RestController
@RequestMapping("/mag")
public class OzonBerriesController {
    @Autowired
    private OzonBerriesService ozBer;

    @PostMapping("/reg")
    public String registration(@RequestBody UserRegistrationRequestDto userRegistrationRequestDto) {
        return ozBer.registration(userRegistrationRequestDto);
    }

    @GetMapping("/createCategory")
    public String createCategory(@RequestParam String categoryname,
                                  @RequestParam(required = false) String description,
                                  @RequestParam(required = false) Integer parentId,
                                  @RequestParam Integer shopId) {
        return ozBer.createCategory(categoryname, description, parentId, shopId);
    }


    @GetMapping("/createProduct")
    public String createProduct(@RequestParam String productName,
                                 @RequestParam(required = false) String description,
                                 @RequestParam Integer categoryId,
                                 @RequestParam Integer price,
                                 @RequestParam Integer count) {
        return ozBer.createProduct(productName, description, categoryId, price, count);
    }


    @GetMapping("/searchProducts")
    public List<Product> searchProducts(@RequestParam(required = false) String productName,
                                        @RequestParam(required = false) Integer categoryId,
                                        @RequestParam(required = false) Integer priceMin,
                                        @RequestParam(required = false) Integer priceMax) {
        return ozBer.searchProducts(productName, categoryId,
                priceMin, priceMax);
    }


    @GetMapping("/searchShops")
    public List<Shop> searchShop(@RequestParam(required = false) String shopName){
        return ozBer.searchShop(shopName);
    }


    @GetMapping("/searchParentOrCategory")
    public List<Category> searchParentOrCategory(@RequestParam Integer shopId,
                                                 @RequestParam(required = false) Integer parentId){
        return ozBer.searchParentOrCategory(shopId, parentId);
    }




    @PostMapping("/addToBasket")
    public String addToBasket(@RequestParam Integer productId, @RequestParam Integer userId){

        return ozBer.addToBasket(productId, userId);
    }


    @GetMapping("/basket")
    public List<Basket> basket(@RequestParam Integer userId){

        return ozBer.basket(userId);
    }

    @DeleteMapping("/deleteBasket")
    public void basketRemove(@RequestParam Integer productId, @RequestParam Integer userId){
         ozBer.basketRemove(productId, userId);
    }


    @DeleteMapping("/deleteProduct")
    public Basket productRemove(@RequestParam Integer productId, @RequestParam Integer userId){
        return ozBer.productRemove(productId, userId);
    }


    @GetMapping("/buy")
    public String buyGoods(@RequestParam Integer userId){
        return ozBer.buyGoods(userId);
    }


    @GetMapping("/selectStatusMagazine")
    public Shop.ShopStatus shopStatus(@RequestParam Integer shopId, @RequestParam Integer userId,
                             @RequestParam Shop.ShopStatus status){
        return ozBer.shopStatus(shopId, userId, status);
    }


    @GetMapping("/userStatus")
    public User.UserStatus userDelete(@RequestParam Integer userId,
                             @RequestParam User.UserStatus status){
        return ozBer.userStatus(userId, status);
    }


    @DeleteMapping("/categoryDelete")
    public String categoryDelete(@RequestParam Integer categoryId){
        return ozBer.categoryDelete(categoryId);
    }











}
