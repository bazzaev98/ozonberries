package ru.bazzaev.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRegistrationRequestDto {

    String email;
    String password;
    String roleName;
    String shopName;
    String description;
    String name;
}

