package ru.bazzaev.dto;

import lombok.Getter;
import lombok.Setter;




@Getter
@Setter
public class ProductDto {

    private String name;
    private String description;
    private Integer categoryId;
    private Integer price;
    private String categoryName;
    private String shopName;
    private String shopId;


}
