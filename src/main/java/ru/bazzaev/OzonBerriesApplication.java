package ru.bazzaev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class OzonBerriesApplication {
    public static void main(String[] args) {
        SpringApplication.run(OzonBerriesApplication.class, args);
    }
}
