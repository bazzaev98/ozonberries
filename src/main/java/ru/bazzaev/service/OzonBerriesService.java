package ru.bazzaev.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.bazzaev.dto.UserRegistrationRequestDto;
import ru.bazzaev.model.*;
import ru.bazzaev.repository.*;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;


@Service
public class OzonBerriesService {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserToRoleRepository userToRoleRepository;
    @Autowired
    private ShopRepository shopRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private BasketRepository basketRepository;
    @Autowired
    private BuyRepository buyRepository;

    @Transactional
    public String registration(UserRegistrationRequestDto userRegistrationRequestDto) {

        if (userRepository.findByEmail(userRegistrationRequestDto.getEmail()).isPresent()) {
            throw new RuntimeException("пользователь с такой почтой уже зарегистрирован");
        }


        Role role = roleRepository.findByName(lower(userRegistrationRequestDto.getRoleName()))
                .orElseThrow(() -> new EntityNotFoundException("роль не найдена"));


        User user = new User();
        user.setEmail(userRegistrationRequestDto.getEmail());
        user.setUsername(userRegistrationRequestDto.getName());
        user.setStatus(User.UserStatus.ACTIVE);
        user.setPassword(passwordEncoder.encode(userRegistrationRequestDto.getPassword()));
        userRepository.save(user);

        if (userToRoleRepository.findByUserIdAndRoleId(user.getId(), role.getId()).isPresent()) {
            throw new RuntimeException("данный пользователь с такой ролью уже зарегистрирован");
        }
        UserToRole userToRole = new UserToRole();
        userToRole.setRoleId(role.getId());
        userToRole.setUserId(user.getId());
        userToRoleRepository.save(userToRole);

        if(RoleRepository.SELLER.equals(lower(userRegistrationRequestDto.getRoleName()))){
          if(shopRepository.findByName(userRegistrationRequestDto.getShopName()).isPresent()){
              throw new RuntimeException("магазин с таким названием уже существует.");
          }
          Shop shop = new Shop();
          shop.setName(userRegistrationRequestDto.getShopName());
          shop.setDescription(userRegistrationRequestDto.getDescription());
          shop.setUser(user);
          shop.setStatus(Shop.ShopStatus.ACTIVE);
          shopRepository.save(shop);
            return "Вы успешно прошли регистрацию" + "You shopId: " + shop.getId() + " UserId: " +  user.getId();
        }

        return "Вы успешно прошли регистрацию.";
    }

    public String createCategory(String categoryname, String description,
                                 Integer parentId, Integer shopId){
        if(categoryRepository.findByNameAndShop(categoryname, shopId).isPresent()){
            throw new RuntimeException("данная категория уже существует.");
        }
        Category category = new Category();
        category.setName(categoryname);
        category.setDescription(description);
        category.setParentId(parentId);
        category.setShop(shopRepository.getById(shopId));
        categoryRepository.save(category);
        return  "Категория успешно создана. CategoryId = " + category.getId();
    }

    public String createProduct(String productname, String description,
                                 Integer categoryId, Integer price,
                                 Integer count){
        if(productRepository.findByNameAndCategoryId(productname, categoryId).isPresent()){
            throw new RuntimeException("данный товар уже внесен.");
        }

        Product product = new Product();
        Category category = new Category();
        category.setId(categoryId);
        product.setName(productname);
        product.setDescription(description);
        product.setCategory(category);
        product.setPrice(price);
        product.setCount(count);
        product.setStatus(Product.ProductStatus.ACTIVE);
        productRepository.save(product);

        return "Продукт создан. ProductId: " + product.getId();

    }

    public List<Product> searchProducts(String productName, Integer categoryId,
                                        Integer priceMin, Integer priceMax){

        return productRepository.findByFilter(lower(productName), categoryId, priceMin, priceMax);
    }

    public List<Shop> searchShop(String shopName){

        return shopRepository.findByNames(lower(shopName));
    }
    public List<Category> searchParentOrCategory(Integer shopId, Integer parentId){

            return categoryRepository.findByShopOrCategoryId(shopId, parentId);
    }



    @Transactional
    public String addToBasket(Integer productId, Integer userId){
      if((basketRepository.findByProduct(productId, userId)).isPresent()) {
          int count = basketRepository.findByBasketProduct(productId, userId).getCount();
          Basket basket = basketRepository.findByBasketProduct(productId, userId);
          basket.setCount(count + 1);
          basketRepository.save(basket);
          Product product = productRepository.findByProductId(productId);
          product.setCount(product.getCount() - 1);
          productRepository.save(product);

      }
      else {
         if(productRepository.findByProductId(productId) == null){
             throw new RuntimeException("Продукта с данным id не существует.");
         }
          if(userRepository.findByUserId(userId) == null){
              throw new RuntimeException("Пользователя с данным id не существует.");
          }
          Basket basket = new Basket();
          basket.setCount(1);
          basket.setProduct(productRepository.findByProductId(productId));
          basket.setUser(userRepository.findByUserId(userId));
          basketRepository.save(basket);
          Product product = productRepository.findByProductId(productId);
          product.setCount(product.getCount() - 1);
          productRepository.save(product);
      }
        return "Товар успешно добавлен в корзину.";
    }

    public List<Basket> basket(Integer userId){
        if(basketRepository.findByBasket(userId).isEmpty()){
            throw new RuntimeException("Пользователя с данным id не существует.");
        }
        else {
            return basketRepository.findByBasket(userId);
        }
    }

    @Transactional
    public void basketRemove(Integer productId, Integer userId){
        List <Basket> list = basketRepository.findByUserId(userId, productId);
        if (list.isEmpty()){
            throw new RuntimeException("Указаны неверные данные.");
        }
        for (int i = 0; i < list.size(); i++) {
            Product product = productRepository.findByProductId(list.get(i).getProduct().getId());
            product.setCount(product.getCount() + list.get(i).getCount());
            productRepository.save(product);
            basketRepository.delete(list.get(i));
        }
    }

    @Transactional
    public Basket productRemove(Integer productId, Integer userId){
       if(basketRepository.removeByBasket(productId, userId) == null){
           throw new RuntimeException("Продукта с данным id не существует.");
       }
        Basket a = basketRepository.removeByBasket(productId, userId);
        if (a.getCount() > 1){
            a.setCount(a.getCount() - 1);
            basketRepository.save(a);
            Product product = productRepository.findByProductId(productId);
            product.setCount(product.getCount() + 1);
            productRepository.save(product);
            return a;
        }
        else{
            basketRepository.delete(a);
            Product product = productRepository.findByProductId(productId);
            product.setCount(product.getCount() + 1);
            productRepository.save(product);
            return null;
        }
    }

    @Transactional
    public String buyGoods(Integer userId){
        if(basketRepository.findByBasket(userId).isEmpty()){
            throw new RuntimeException("Пользователя с данным id не существует.");
        }
        List <Basket> list = basketRepository.findByBasket(userId);
        for (Basket b : list) {
            Buy buy = new Buy();
            buy.setProduct(b.getProduct());
            buy.setUser(b.getUser());
            buy.setCount(b.getCount());
            buyRepository.save(buy);
            basketRepository.delete(b);
        }
            return "Покупка прошла успешно.";
    }


    public Shop.ShopStatus shopStatus(Integer shopId, Integer userId, Shop.ShopStatus shopStatus){
      if(shopRepository.findByUserAndId(shopId, userId).isEmpty()){
          throw new RuntimeException("Магазина с данными id не существует.");
      }
        List<Shop> shops = shopRepository.findByUserAndId(shopId, userId);
      List<Product> products = productRepository.findByFilterProductShop(shopId);
        Product.ProductStatus prodStatus = status(shopStatus);
      for (Shop shop : shops){
           shop.setStatus(shopStatus);
           shopRepository.save(shop);
        }
            for (Product product : products) {
                product.setStatus(prodStatus);
                productRepository.save(product);
            }
                return shopStatus;
        }

        @Transactional
    public User.UserStatus userStatus(Integer userId, User.UserStatus status){
            if(userRepository.findByUserId(userId) == null){
                throw new RuntimeException("Пользователя с данным id не существует.");
            }
            User user = userRepository.findByUserId(userId);
            user.setStatus(status);
            userRepository.save(user);
            Product.ProductStatus prodStat = prodStatus(status);
            Shop.ShopStatus shopStat = shopStatus(status);
            List <Shop> shopList = shopRepository.findByUserId(userId);
            for (int i = 0; i < shopList.size(); i++) {
                List <Category> categoryList = categoryRepository.findByShopId(shopList.get(i).getId());
                shopList.get(i).setStatus(shopStat);
                shopRepository.save(shopList.get(i));
                for (int j = 0; j < categoryList.size() ; j++) {
                    List<Product> productList = productRepository.findByCategoryId(categoryList.get(j).getId());
                    for (int k = 0; k < productList.size(); k++) {
                        productList.get(k).setStatus(prodStat);
                        productRepository.save(productList.get(k));
                    }
                }
            }
        basketRepository.deleteByUserId(userId);
        return status;
    }

    @Transactional
    public String categoryDelete(Integer categoryId){
        if(categoryRepository.findByCategoryAndParentCategory(categoryId).isEmpty()){
            throw new RuntimeException("Категории с данными id не существует.");
        }
        List<Integer> categories = categoryRepository.findByCategoryAndParentCategory(categoryId);
        for (int i = categories.size() - 1; i >= 0; i--) {
            int category = categories.get(i);
            productRepository.deleteByCategoryId(category);
            categoryRepository.deleteByCategoryId(category);
        }

        return "Category deleted.";
    }




    private String lower(String line){
        if(line == null){
            return null;
        }
        return line.toLowerCase();
    }

    private Product.ProductStatus status(Shop.ShopStatus shop){
        if(shop.equals(Shop.ShopStatus.ACTIVE)){
            return Product.ProductStatus.ACTIVE;
        }
        return Product.ProductStatus.PASSIVE;
    }

    private Product.ProductStatus prodStatus (User.UserStatus status){
        if(status.equals(User.UserStatus.ACTIVE)){
            return Product.ProductStatus.ACTIVE;
        }
        return Product.ProductStatus.PASSIVE;
    }

    private Shop.ShopStatus shopStatus(User.UserStatus status){
        if(status.equals(User.UserStatus.ACTIVE)){
            return Shop.ShopStatus.ACTIVE;
        }
        return Shop.ShopStatus.PASSIVE;
    }

}
